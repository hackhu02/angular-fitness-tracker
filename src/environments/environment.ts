// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAB2ca5sAn1JOUFd5CGHjR8dR3PFtDEf5A",
    authDomain: "ng-fitness-tracker-ac505.firebaseapp.com",
    databaseURL: "https://ng-fitness-tracker-ac505.firebaseio.com",
    projectId: "ng-fitness-tracker-ac505",
    storageBucket: "ng-fitness-tracker-ac505.appspot.com",
    messagingSenderId: "455500133170"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
