import { Exercise } from "./exercise.model";
import { Subject } from 'rxjs'
import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { map } from "rxjs/operators";
import { Subscription } from 'rxjs'
import { UIService } from "../shared/ui.service";

@Injectable()
export class TrainingServcie {

  exerciseChanged = new Subject<Exercise>();
  exercisesChanged = new Subject<Exercise[]>();
  finishedExercisesChanged = new Subject<Exercise[]>();

  private availableExercises: Exercise[] = []
  private runningExercise: Exercise

  private fbSubs: Subscription[] = []

  constructor(
    private db:AngularFirestore,
    private uiService: UIService
  ) {}

  fetchAvailableExercises() {
    this.fbSubs.push(this.db
      .collection('availableExercises')
      .snapshotChanges()
      .pipe(map(docData => {
        return docData.map(doc => {
          const  name = doc.payload.doc.data()['name']
          const  duration = doc.payload.doc.data()['duration']
          const  calories = doc.payload.doc.data()['calories']
          return {
            id: doc.payload.doc.id,
            name: name,
            duration: duration,
            calories: calories
          }})
        })
      ).subscribe((exercises: Exercise[]) => {
        this.availableExercises = exercises
        this.exercisesChanged.next([...this.availableExercises])
      }, error => {
        this.uiService.loadingStateChanged.next(false)
        this.uiService.snackBarTime('Fetching Exercises failed, please try again later', null, 3000)
      })
    )
  }

  startExercise(selectedId: string) {
    // this.db.doc('availableExercises/' + selectedId).update({lastSelected: new Date()})
    this.runningExercise = this.availableExercises.find( exercise => exercise.id === selectedId)
    this.exerciseChanged.next({...this.runningExercise})
  }

  completeExercise() {
    this.addDataToDatabase({
      ...this.runningExercise,
      date: new Date(),
      state: 'completed'
    })
    this.runningExercise = null
    this.exerciseChanged.next(null)
  }

  cancelExercise(progress: number) {
    this.addDataToDatabase({
      ...this.runningExercise,
      duration: this.runningExercise.duration * ( progress / 100),
      calories: this.runningExercise.calories * ( progress / 100),
      date: new Date(),
      state: 'cancelled'
    })
    this.runningExercise = null
    this.exerciseChanged.next(null)
  }

  getRunningExercise() {
    return { ...this.runningExercise }
  }

  fetchCompletedOrCancelledExercises() {
    this.fbSubs.push(this.db
      .collection('finishedExercises')
      .valueChanges()
      .subscribe( (exercises: Exercise[]) => {
        this.finishedExercisesChanged.next(exercises)
      })
    )
  }

  cancelSubscriptions() {
    this.fbSubs.forEach(sub => sub.unsubscribe())
  }

  private addDataToDatabase(exercise: Exercise) {
    this.db.collection('finishedExercises').add(exercise)
  }
}