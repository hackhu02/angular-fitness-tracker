import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs'
import { TrainingServcie } from './training.service';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.sass']
})
export class TrainingComponent implements OnInit {

  ongoingTraining = false;
  exerciseSubscription: Subscription;

  constructor(private trainingService:TrainingServcie) { }

  ngOnInit() {
    this.exerciseSubscription = this.trainingService.exerciseChanged.subscribe( exercise => {
      if (exercise) {
        this.ongoingTraining = true
      } else {
        this.ongoingTraining = false
      }
    })
  }

}
