import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { Exercise } from '../exercise.model';
import { TrainingServcie } from '../training.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-past-trainings',
  templateUrl: './past-training.component.html',
  styleUrls: ['./past-training.component.sass']
})
export class PastTrainingComponent implements OnInit, AfterViewInit{

  displayedColumns = ['date', 'name', 'duration', 'calories', 'state']
  dataSource = new MatTableDataSource<Exercise>();
  private exChangedSubscription: Subscription;

  database: Exercise[]

  @ViewChild(MatSort) sort: MatSort
  @ViewChild(MatSort) paginator: MatPaginator

  constructor(private trainingService: TrainingServcie) { }

  ngOnInit() {
    this.exChangedSubscription = this.trainingService.finishedExercisesChanged.subscribe(
      (exercises: Exercise[]) => {
        this.dataSource.data = [...exercises]
        this.database = [...exercises]
        console.log('past: ', this.dataSource.data)
      })
    this.trainingService.fetchCompletedOrCancelledExercises()
  }

  ngOnDestroy() {
    this.exChangedSubscription.unsubscribe()
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort
    this.dataSource.paginator = this.paginator
  }

  doFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase()
  }

}
