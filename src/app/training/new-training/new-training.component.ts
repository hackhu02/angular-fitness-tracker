import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';
import { TrainingServcie } from '../training.service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs'
import { Exercise } from '../exercise.model';
import { UIService } from 'src/app/shared/ui.service';

@Component({
  selector: 'app-new-training',
  templateUrl: './new-training.component.html',
  styleUrls: ['./new-training.component.sass']
})
export class NewTrainingComponent implements OnInit, OnDestroy {

  exercises: Exercise[]
  exercisesSubscription: Subscription
  exercisesIsChangedSubscription: Subscription
  isLoading: boolean

  @Output() trainingStart = new EventEmitter<void>();

  constructor(
    private trainingService: TrainingServcie,
    private uiService: UIService
  ) {}

  ngOnInit() {
    this.fetchExercisesAgain()
    this.isLoading = true
    this.exercisesSubscription = this.trainingService.exercisesChanged.subscribe( exercises => {
      this.exercises = exercises
      this.isLoading = false
    })
    this.exercisesIsChangedSubscription = this.uiService.loadingStateChanged.subscribe( isLoading => {
      this.isLoading = isLoading
    })
  }

  fetchExercisesAgain() {
    this.uiService.loadingStateChanged.next(true)
    this.trainingService.fetchAvailableExercises()
  }

  ngOnDestroy() {
    this.exercisesSubscription.unsubscribe()
    this.exercisesIsChangedSubscription.unsubscribe()
  }

  onStartTraining(form: NgForm) {
    this.trainingService.startExercise(form.value.exercise)
  }

}
